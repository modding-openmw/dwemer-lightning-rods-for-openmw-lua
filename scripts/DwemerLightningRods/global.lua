local core = require('openmw.core')
if core.API_REVISION < 39 then
    error("This requires OpenMW 0.49 or newer! https://openmw.org/downloads/")
end

if not core.contentFiles.has("OAAB_Data.esm") then
    error("This requires OAAB_Data.esm! https://www.nexusmods.com/morrowind/mods/49042")
end

if core.contentFiles.has("Dwemer Lightning Rods - OpenMW.ESP") then
    print()
    print()
    print("          !!!! WARNING !!!!")
    print("The 'Dwemer Lightning Rods - OpenMW.ESP' plugin was detected in your load order -- it is not required when you have this loaded, please disable it!")
    print("          !!!! WARNING !!!!")
    print()
    print()
end

local types = require('openmw.types')
local v3 = require('openmw.util').vector3
local world = require('openmw.world')

-- These were taken from the MWSE code that ships with Dwemer Lightning Rods
local srcId = "ex_dwrv_steamstack00"
local destId = "AB_Ex_DwrvSteamstackRod_a"
local offset = v3(-128, 0, 1536)
local minAngle = 25
local maxAngle = 360 - 25 -- 335

local function replace(obj)
    if not obj.type ~= types.Static and obj.recordId ~= srcId then
        -- This isn't the right type or recordId
        return false
    end
    local angle = math.deg(math.abs(obj.position.y))
	return (obj.cell.isExterior or obj.cell:hasTag("QuasiExterior"))
        and not ((angle > minAngle) and (angle < maxAngle))
end

local function onObjectActive(obj)
    if replace(obj) == true then
        local ab = world.createObject(destId)
        ab.enabled = obj.enabled
        ab:setScale(obj.scale)
        ab:teleport(obj.cell, obj.position + obj.rotation * offset, obj.rotation)
        obj:remove()
    end
end

return { engineHandlers = { onObjectActive = onObjectActive } }
