# Dwemer Lightning Rods for OpenMW-Lua

Automatically replace vanilla Dwemer steam stacks with OAAB ones that use the lightning effect. Compatible with everything.

**Requires OpenMW 0.49 or newer and [OAAB Data](https://www.nexusmods.com/morrowind/mods/49042)!**

#### Credits

**[Dwemer Lightning Rods](https://www.nexusmods.com/morrowind/mods/50236) was created by** Melchior Dahrk

**OpenMW-Lua script by:**

* johnnyhostile
* zackhasacat
* ezze

#### Web

[Project Home](https://modding-openmw.gitlab.io/dwemer-lightning-rods-for-openmw-lua/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/dwemer-lightning-rods-for-openmw-lua)

#### Installation

**Requires OpenMW 0.49 or newer and OAAB Data!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/dwemer-lightning-rods-for-openmw-lua/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Architecture\dwemer-lightning-rods-for-openmw-lua

        # Linux
        /home/username/games/OpenMWMods/Architecture/dwemer-lightning-rods-for-openmw-lua

        # macOS
        /Users/username/games/OpenMWMods/Architecture/dwemer-lightning-rods-for-openmw-lua

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Architecture\dwemer-lightning-rods-for-openmw-lua"`)
1. Add `content=Dwemer Lightning Rods.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher

Note: Because the Ashlands, Molag Amur and Red Mountain regions (where a lot of dwemer ruins can be found) have no chance of thunderstorms in the vanilla game, you will only be able to experience active lightning rods there with weather affecting mods. I recommend [Solthas Blight Weather Pack](https://www.nexusmods.com/morrowind/mods/52354).

Please also note that the original [Dwemer Lightning Rods](https://www.nexusmods.com/morrowind/mods/50236) by Melchior Dahrk is not required when using this. If you have it loaded a warning will be printed to the log/console.

#### Known Issues

* Lua-created objects do not have distant statics ([OpenMW bug link](https://gitlab.com/OpenMW/openmw/-/issues/7452))

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/dwemer-lightning-rods-for-openmw-lua/-/issues)
* Email `dwemer-lightning-rods-for-openmw-lua@modding-openmw.com`
* Contact the author on Discord: `johnnyhostile#6749`
* Contact the author on Libera.chat IRC: `johnnyhostile`
