## Dwemer Lightning Rods for OpenMW-Lua Changelog

#### Version 1.6

* Optimization: the mod now filters unwanted object IDs before doing math-related things

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/23932743)

#### Version 1.5

* Fixed a problem with how we implemented copying over scale

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/23716397)

#### Version 1.4

* Scale is now preserved when replacing

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/23716106)

#### Version 1.3

* The mod now prints a warning if `Dwemer Lightning Rods - OpenMW.ESP` is found in the load order
* Removed leftover player script definition that caused the mod to instacrash

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/19122035)

#### Version 1.2

* Fixed documentation

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/19095941)

#### Version 1.1

* Fixed packaging error that prevented 1.0 from ever releasing

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/19079036)

#### Version 1.0

Initial version of the mod.
